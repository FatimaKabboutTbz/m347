# M346

## A

![getting_started_website](./getting_started_website.jpg)
![docker_container](./docker_container.jpg)

## B

### Commands

- docker --version
- docker search ubuntu
- docker search nginx
- docker pull nginx
- docker create -p 8081:80 nginx
- docker start 7804f357ea4ff90950083523292a323a6626b11d1e17d21126c4e74847aa9f92
- docker run -d
- docker run -d -p 8082:80 --name kn01_ubuntu ubuntu
  - Der Container konnte erstellt werden. Das Image wurde neu gepullt, da es eine neuere Version gab. Es wurde auch die ID des neu erstellten Containers ausgegeben.
- docker run -it --name kn01_ubuntu_v2 ubuntu
  - Auch wie der vorherige command konnte mein Container erstellt und gestartet werden. Das image musste nicht neu gepullt werden, da ich es schon geholt hatte. Da ich die parameter -it mitgab, öffnete sich gleich das Shell im Container selbst. Hier kann ich jetzt Bash commands ausführen und mein Container verwalten.
- exit
- docker exec -it goofy_kowalevski /bin/bash
- service nginx status
- exit
- docker ps
- docker stop goofy_kowalevski
- docker stop festive_maxwell
- docker rm goofy_kowalevski festive_maxwell kn01_ubuntu kn01_ubuntu_v2
- docker rmi nginx
- docker rmi ubuntu

### Images

![nginx_website](./nginx_website.jpg)
![nginx_command](./nginx_command.jpg)
![docker_ps](./docker_ps.jpg)

## C

![docker_hub_repo](./docker_hub_repo.jpg)

## D

### Commands

- docker pull nginx
- docker tag nginx:latest fkabbtbz/m347:nginx
  - Dieser Befehl benennt das nginx:latest-Image um und kennzeichnet es neu als fkabbtbz/m347:nginx. Ein "Tag" in Docker ist eine Bezeichnung, die verwendet wird, um verschiedene Versionen oder Varianten desselben Images zu unterscheiden.
- docker login -u fkabbtbz -p
- docker tag nginx:latest fkabbtbz/m347:nginx
  - Dieser Befehl pushed mein neu erstellten Tag auf meinem Repository m347.
- docker pull mariadb
- docker tag mariadb:latest fkabbtbz/m347:mariadb
- docker push fkabbtbz/m347:mariadb

### Images

![repo_tags](./repo_tags.jpg)
