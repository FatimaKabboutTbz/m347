# KN08 - M346

![](./)

### Datenbank

![db](./db.jpg)

### FE starten

![fe_starten](./fe_starten.jpg)

### FE Docker erstellen

![fe_docker](./creating_web_docker.jpg)

### FE mit Docker starten

![fe_container_web](./fe_container_web.jpg)

### Account Docker erstellen

![creating_be_docker](./creating_be_docker.jpg)

### Account Swagger

![swagger](./swagger.jpg)

### Microservices testen

![website_with_data](./website_with_data.jpg)

### BuySell SendReceive testen

![buysell_sendreceive_requests](./buysell_sendreceive_requests.jpg)
