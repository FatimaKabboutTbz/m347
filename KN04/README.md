# M346

## A

### Teil a

### Commands

- docker compose up -d

![info_php](./info_php.jpg)
![db_php](./db_php.jpg)

### Teil b

#### Fehler Erklärung

Es tritt einen Fehler auf, da meine credentials in der Docker Compose Datei und die in meiner Image, nicht übereinstimmen, was heisst das ich keinen Zugriff auf meiner Datenbank habe. Um dieses Problem zu lösen, müsste ich die credentials so anpassen, damit die dann auch stimmen.

### Images

![info_php_teilb](./info_php_teilb.jpg)
![db_php_teilb](./db_php_teilb.jpg)

## B

![info_aws](./info_aws.jpg)
![db_aws](./db_aws.jpg)
