# M346

## A

### Commands

- docker build -t fkabbtbz/m347:kn02a .
- docker push fkabbtbz/m347:kn02a
- docker run -d -p 8083:80 --name kn02a_test fkabbtbz/m347:kn02a

### Images

![html_website](./html_website.jpg)
![kn02a_image](./kn02a_image.jpg)

## B

### Commands DB

- docker login -u fkabbtbz -p
- docker build -t fkabbtbz/m347:kn02b-db -f dockerfileDB .
- docker push fkabbtbz/m347:kn02b-db
- docker run -d -p 3306:3306 --name kn02b-db fkabbtbz/m347:kn02b-db

### Commands Web

- docker build -t fkabbtbz/m347:kn02b-web -f dockerfileWeb .
- docker push fkabbtbz/m347:kn02b-web
- docker run -d -p 8080:80 --name kn02b-web --link kn02b-db fkabbtbz/m347:kn02b-web

### Images

![telnet_command](./telnet_command.jpg)
![info_php_website](./info_php_website.jpg)
![db_php](./db_php.jpg)
