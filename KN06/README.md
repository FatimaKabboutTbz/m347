# M346

## A

### Commands

![clusters](./clusters.jpg)

## B

#### Node 1

![cmd_node_1](./cmd_node_1.jpg)

#### Node 2

![cmd_node_2](./cmd_node_2.jpg)

#### Microk8s Status

![micro_status](./micro_status.jpg)

- _microk8s is running_: zeigt an das microk8s läuft
- _High-availability: yes_: High-Availability ist an d.h., dass auch im Falle von Hardware- oder Softwarefehler der Cluster weiterhin funktioniert.
- _Datastore master nodes_: IP-Adressen und Ports vom Datastore von den Master-Nodes. Dienen zur Speicherung von Konfigurationsinformationen und weitere Daten des Clusters.
- _Datastore standby nodes: none_: Es gibt keine Standby-Nodes. Standby-Nodes können als Backup oder zur Lastverteilung dienen.

#### Remove Node

- `microk8s leave`
- `microk8s remove-node 172.31.110.20`

![remove_node_p1](./remove_node_p1.jpg)
![remove_node_p2](./remove_node_p2.jpg)

#### Microk8s Status Pt. 2

![node_to_worker](./node_to_worker.jpg)
![micro_status_2](./micro_status_2.jpg)
Der Unterschied liegt darin, dass jetzt der Cluster nicht mehr HA hat. Das liegt daran das eine von den Nodes jetzt eine Worker Node ist und diese laufen nicht auf der Kubernetes control plane. Dies beudeutet das sie nicht zur HA beitragen und um die HA zu aktivieren, muss man im Cluster mindestens 3 Nodes haben.

#### Master - get nodes | Worker - get nodes

![master_getter](./master_getter.jpg)
![worker_getter](./worker_getter.jpg)
Da eben die Worker-Node nicht auf der control plane arbeitet, hat sie auch keinen Status. Somit kann das nur vom Master Node abgerufen werden.

#### Unterschied zwischen microk8s und microk8s kubectl

Mit `microk8s` kann man einzelne Nodes in einem Cluster verwalten und bearbeiten. Wobei man mit `kubectl` ganze Clusters, mit mehreren Nodes, bearbeiten kann. Dafür nutzt man `microk8s kubectl`.
