# M346

## A

### Commands

- docker network create tbz --subnet 172.19.0.0/16
- docker run -it -d --name=busybox1 busybox
- docker run -it -d --name=busybox2 busybox
- docker run -it -d --network=tbz --name=busybox3 busybox
- docker run -it -d --network=tbz --name=busybox4 busybox

### Welche Adressen haben die vier Container?

- docker inspect busybox1 busybox2 busybox3 busybox4
- busybox1: 172.17.0.2
- busybox2: 172.17.0.3
- busybox3: 172.19.0.2
- busybox4: 172.19.0.3

### Tests auf Busybox 1

- ifconfig -a
- ping -c 4 busybox2, ping -c 4 172.17.0.3
- ping -c 4 busybox3, ping -c 4 172.19.0.2

### Tests auf Busybox 3

- ifconfig -a
- ping -c 4 busybox1, ping -c 4 172.17.0.2
- ping -c 4 busybox4, ping -c 4 172.19.0.3

### Images

![commands](./commands.jpg)
![busybox1](./busybox1.jpg)
![busybox3](./busybox3.jpg)

Der Unterschied besteht darin, dass bei einem default bridge network Container nur teilweise verbunden sind, nämlich über die IP-Adresse, die sich im Laufe der Zeit ändern kann. Wenn ich jedoch mein eigenes Netzwerk erstelle, kann ich Container-Namen verwenden, die sich im Laufe der Zeit nicht ändern.

In KN02 waren die Container im gleichen Gateway, konnten aber nicht über den Container-Namen kommunizieren. Mit dem Link (--link) verknüpfe ich den Namen mit der IP-Adresse, was es dem Container ermöglicht, zu kommunizieren.
