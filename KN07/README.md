# M346

## A

#### Unterschied zwischen _Pods_ und _Replicas_

Pods sind die kleinsten ausführbaren Einheiten in Kubernetes und können einen oder mehrere Container enthalten, die zusammen auf einem Cluster laufen. Ein Replica stellt sicher, dass eine festgelegte Anzahl identischer Pods gleichzeitig ausgeführt wird, um die Verfügbarkeit und Skalierbarkeit einer Anwendung zu gewährleisten. Einfach gesagt bedeutet dies, dass Pods die Anwendungsinstanz sind, während ein Replica dafür sorgt das genügend Pods laufen, um die Anwendun am laufen zu halten.

#### Unterschied zwischen _Service_ und _Deployment_

Ein Service ist wie eine feste Adresse für eine Gruppe von Pods in Kubernetes. Es sorgt dafür, dass andere Teile der Anwendung mit diesen Pods kommunizieren können, ohne sich Gedanken über ihre genauen Standorte machen zu müssen. Es hilft auch dabei, den Verkehr gleichmässig auf diese Pods zu verteilen.

Ein Deployment ist eine Anweisung darüber, wie Pods in Kubernetes erstellt und aktualisiert werden sollen. Es ermöglicht es, Änderungen an der Anwendung zu machen, während sie weiter läuft. Zum Beispiel kann es neue Pods hinzufügen oder alte ersetzen, ohne dass die Anwendung für Benutzer offline geht.

#### Welches Problem löst _Ingress_?

Ingress ist wie ein Tor für den Verkehr von aussen in einen Kubernetes-Cluster. Es nimmt Anfragen von ausserhalb des Clusters entgegen und leitet sie an die richtigen Dienste innerhalb des Clusters weiter, basierend auf vordefinierten Regeln. Ohne Ingress müssten alle Dienste im Cluster eigene öffentliche IP-Adressen und Ports haben, was unpraktisch und unübersichtlich wäre.

#### Für was ist ein _statefulset_?

Ein StatefulSet in Kubernetes hilft bei der Verwaltung von Anwendungen, die sich an bestimmte Zustände erinnern müssen, wie z. B. der momentan eingeloggte User oder andere Daten, die über längere Zeit gespeichert werden müssen. Im Gegensatz zu Anwendungen, die keine dauerhaften Daten benötigen und einfach dupliziert werden können, müssen stateful Anwendungen bestimmte Daten speichern, die stabil und persistent sind. Zum Beispiel könnte ein StatefulSet für ein Messaging-System verwendet werden, bei dem die Nachrichten auf jedem Node gespeichert sind, um sicherzustellen, dass sie nicht verloren gehen.

## B

![apply_cmd](./apply_cmd.jpg)
![getall_cmd](./getall_cmd.jpg)
![secret_config_cmd](./secret_config_cmd.jpg)

Der Unterschied liegt beim Deployment und Service, da wir die in Teil B sie in einer Datei zusammengefasst haben. Es wäre auch möglich gewesen diese Seperat zu halten.

### MongoURL

Der angegebene MongoURL im `mongo-config.yaml` war korrekt, da wie den definierten Namen im `mongo-service` verwendeten. Der Name im `mongo-service` zeigt also auf die MongoDB.

### microk8s kubectl describe

#### Master

![master_describe](./master_describe.jpg)

#### Node 1

![node1_describe](./node1_describe.jpg)

#### Mongo Service

![master_db_describe](./master_db_describe.jpg)

Der Unterschied ist, dass anstatt hier die Detail Informationen des webapp-service angezeigt werden, werden die Informationen der Datenbank (MongoDB) gezeigt.

### Aufruf der Webseite

#### Master

![master_website](./master_website.jpg)

#### Node 1

![node1_website](./node1_website.jpg)

Man muss bei der Securitygroup bei den inbound roules den Port 30100 zulassen, den ansonsten kann man die Webseite nicht aufrufen.

### Verbindung mit MongoDB Compass

Das Verbinden zur Datenbank funktioniert nicht, weil der Port vom AWS nicht veröffentlicht wurde. Zudem, weil auch in unserer Kubernetes Konfiguration wir den Port nicht veröffentlicht haben, um diese Probleme zu lösen, müssen wir in userer DB-YAML Datei und bei AWS die Ports veröffentlichen.

### Service Definition anpassen

- In _web.yaml_ den NodePort (32000) und replicas (3) anpassen.
- Den Befehl `microk8s kubectl apply -f web.yaml` nochmals ausführen.
- Änderungen entweder mit Aufruf der Website testen oder mit dem cmd `microk8s kubectl describe service webapp-service`

![node2_website](./node2_website.jpg)
