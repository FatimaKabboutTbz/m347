# M346

## A

### Commands

- docker run --name kn05a -p 8090:80 -v C:/Repository/TBZ/m347/KN05/kn05a/:/usr/share/nginx/html/kn05a -d nginx:latest

![kn05a_script](./kn05a_script.jpg)

## B

### Commands

- docker volume create kn05b-volume
- docker run --name kn05b-1 -p 8070:80 -v kn05b-volume:/usr/share/nginx/html/kn05b -d nginx:latest
- docker run --name kn05b-2 -p 8060:80 -v kn05b-volume:/usr/share/nginx/html/kn05b -d nginx:latest

![kn05b-con1](./kn05b-con1.jpg)
![kn05b-con2](./kn05b-con2.jpg)

## C

![kn05c-con1](./kn05c-con1.jpg)
![kn05c-con2](./kn05c-con2.jpg)
